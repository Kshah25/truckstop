package com.example.kandarpshah.truckstop;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kandarpshah.truckstop.webservice.IGetTruckStops;
import com.example.kandarpshah.truckstop.webservice.RestAdapter;
import com.example.kandarpshah.truckstop.webservice.TruckStopsRs;
import com.example.kandarpshah.truckstop.webservice.TruckStopsRx;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback,LocationListener {

    private GoogleMap mMap;
    private LatLngBounds.Builder bounds = new LatLngBounds.Builder();
    private Marker currentClickedMarker;
    private GoogleApiClient mGoogleApiClient;
    private Location currentLocation;
    LocationManager lm;
    ArrayList<TruckStopsRs> truckStopList = new ArrayList<>();
    static SharedPreferences prefManager = null;
    static final double METERS_TO_MILES = 0.000621371;
    static final int FIVE_SECOND_DELAY = 5000;
    static final int FIFTEEN_SECOND_DELAY = 15000;

    static final String MAP_TYPE = "MAP_TYPE";
    static final String TRACKING_MODE = "TRACKING_MODE";
    Boolean isTrackingMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Dexter.initialize(getApplicationContext());
        lm = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        init();
    }

    private void init(){

        //see if we are in tracking mode or not
        isTrackingMode = Boolean.valueOf(readFromPrefs(MapsActivity.this, TRACKING_MODE));


        //button to toggle between satellite view and map view
        ((Button)findViewById(R.id.mapTypeToggle)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mMap.getMapType() == GoogleMap.MAP_TYPE_SATELLITE)
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                else
                    mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                //persist choice
                saveToPrefs(MapsActivity.this,MAP_TYPE,String.valueOf(mMap.getMapType()));
            }
        });

        //button to center the map
        ((Button)findViewById(R.id.centerMap)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 0));
                }
                //illegal state ex happens if user is trying to center map that has no markers yet.
                //So get the location
                catch(IllegalStateException e){
                    getLocation();
                }
                //security ex happens if user has turned off their location while the app is running.
                //Request to turn on location
                catch(SecurityException e){
                    requestLocationDialog();
                }
            }
        });

        //button to toggle tracking mode
        ((Button)findViewById(R.id.trackingMode)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isTrackingMode) {
                    Toast.makeText(getApplicationContext(), "Tracking Mode Enabled", Toast.LENGTH_LONG).show();
                    isTrackingMode = true;
                    animateMap();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Tracking Mode Disabled", Toast.LENGTH_LONG).show();
                    isTrackingMode = false;
                }
                //persist choice
                saveToPrefs(MapsActivity.this,TRACKING_MODE,String.valueOf(isTrackingMode));
                startUpdatingLocation();
            }
        });



        //button to perform search
        ((Button)findViewById(R.id.search)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //make sure we have results we can search for
                if(truckStopList.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Nothing to search", Toast.LENGTH_LONG).show();
                    return;
                }

                LayoutInflater layoutInflater
                        = (LayoutInflater)getBaseContext()
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                View popupView = layoutInflater.inflate(R.layout.search_layout, null);
                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT,true);

                popupWindow.showAtLocation(((Button)findViewById(R.id.search)), Gravity.CENTER, 0,0);

                final EditText nameEdit = (EditText)(popupWindow.getContentView().findViewById(R.id.srchNameEdit));
                final EditText cityEdit = (EditText)(popupWindow.getContentView().findViewById(R.id.srchCityEdit));
                final EditText stateEdit = (EditText)(popupWindow.getContentView().findViewById(R.id.srchStateEdit));
                final EditText zipEdit = (EditText)(popupWindow.getContentView().findViewById(R.id.srchZipEdit));

                popupWindow.getContentView().findViewById(R.id.doReset).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        populateMap(truckStopList);
                    }
                });
                popupWindow.getContentView().findViewById(R.id.doSearch).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        ArrayList<TruckStopsRs> searchResults = new ArrayList<TruckStopsRs>();
                        for(TruckStopsRs ts: truckStopList){
                            Boolean matches = false;
                            if(zipEdit.getText().toString().length() > 0) {
                                if (ts.getZip().equals(zipEdit.getText().toString().trim()))
                                    matches = true;
                                else
                                    continue;
                            }

                            if(stateEdit.getText().toString().length() > 0) {
                                if (ts.getStateLowerCase().equals(stateEdit.getText().toString().toLowerCase().trim()))
                                    matches = true;
                                else
                                    continue;
                            }

                            if(cityEdit.getText().toString().length() > 0) {
                                if (ts.getCityLowerCase().equals(cityEdit.getText().toString().toLowerCase().trim()))
                                    matches = true;
                                else
                                    continue;
                            }

                            //search for the name should match any part of the string
                            if(nameEdit.getText().toString().length() > 0) {
                                if (ts.getNameLowerCase().contains(nameEdit.getText().toString().toLowerCase().trim()))
                                    matches = true;
                                else
                                    continue;
                            }

                            if(matches)
                                searchResults.add(ts);
                        }
                        populateMap(searchResults);
                    }
                });
            }
        });
    }
    /* Calls the API and gets a list of all truck stops within 100 miles of the current location
     */
    private void searchTruckStops(){
        IGetTruckStops iGetTruckStops = RestAdapter.getRestAdapter().create(IGetTruckStops.class);
        //make request object and populate with current location
        TruckStopsRx TruckStopRequest = new TruckStopsRx(currentLocation.getLatitude(),currentLocation.getLongitude());
        Call<JsonElement> response = iGetTruckStops.getTruckStops(TruckStopRequest,100);
        response.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                if (response.isSuccessful()) {
                    Gson gson = new Gson();
                    try {
                        //need to first convert returned object to arraylist of truck stops
                        truckStopList = gson.fromJson(response.body().getAsJsonObject().get("truckStops"),new TypeToken<ArrayList<TruckStopsRs>>(){}.getType());
                        populateMap(truckStopList);
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }

                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        //if user clicks anywhere on the map, clear out any selected truck stops, if necessary
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if(currentClickedMarker != null){
                    currentClickedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.truck_stop));
                }
                //if user is in tracking mode, pause it for 5 seconds
                if(isTrackingMode){
                    stopUpdatingLocation();
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startUpdatingLocation();
                        }
                    }, FIVE_SECOND_DELAY);
                }
            }
        });

        try {
            mMap.setMapType(Integer.valueOf(readFromPrefs(MapsActivity.this, MAP_TYPE)));
        }
        catch (Exception e){
            //no-op; map type is not set yet
        }
        //make sure app has permissions, and then get the location
        checkPermissions();
    }

    //updates the map when in tracking mode
    private void animateMap(){
        try {
            mMap.clear();
            LatLng myLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            CameraUpdate center =
                    CameraUpdateFactory.newLatLng(myLatLng);
            mMap.moveCamera(center);
            mMap.animateCamera(CameraUpdateFactory.zoomTo(mMap.getMaxZoomLevel()));
            mMap.addMarker(new MarkerOptions().position(myLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.truck)));
        }
        catch (Exception e){ //no-op, nothing to do here
        }
    }

    //updates the map with truck stop markers
    private void populateMap(ArrayList<TruckStopsRs> listOfTruckStops){
       mMap.clear();
        currentClickedMarker = null;
        //first get my location and make a special marker for it
        final LatLng myLocation = new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());
        mMap.addMarker(new MarkerOptions().position(myLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.my_stop)));
        //add all locations to the bounds list
        bounds.include(myLocation);

        //now get all truck stops and add markers to the map for each stop, along with all truck stop info
        for(TruckStopsRs truckStop: listOfTruckStops){
            LatLng tsLatLng = new LatLng(truckStop.getLatitude(),truckStop.getLongitude());
           mMap.addMarker(new MarkerOptions()
                   .title(truckStop.getName())
                   .position(tsLatLng)
                   .snippet(truckStop.getSnippet())
                   .icon(BitmapDescriptorFactory.fromResource(R.drawable.truck_stop)));

            bounds.include(tsLatLng);
        }
        //set a marker click listener for each marker. The color of the marker should change when clicked upon.
        //Also keep track of which marker was most recently clicked, so we can change the marker back to the original color
        //if the user clicks on another marker or somehwere else on the map
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                //if user is viewing this info, pause tracking mode for 15 seconds
                if(isTrackingMode){
                    stopUpdatingLocation();
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startUpdatingLocation();
                        }
                    }, FIFTEEN_SECOND_DELAY);
                }
                if(currentClickedMarker != null){
                    currentClickedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.truck_stop));
                }
                if(marker.getTitle() != null) {
                    marker.showInfoWindow();
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.selected_stop));
                    currentClickedMarker = marker;
                }
                return false;
            }
        });
        //set a custom infowindow adapter to show the title, address, distance and any other info the truck stop might have
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View v = getLayoutInflater().inflate(R.layout.custom_infowindow, null);
                //set title of truck stop
                ((TextView)v.findViewById(R.id.title)).setText(marker.getTitle());
                //get the distance from the current location
                Location tmp = new Location("");
                tmp.setLatitude(marker.getPosition().latitude);
                tmp.setLongitude(marker.getPosition().longitude);
                ((TextView)v.findViewById(R.id.distance)).setText("Distance: " + Math.round(tmp.distanceTo(currentLocation)*METERS_TO_MILES) + " mi");
                //set address, plus any more info that truck stop has
                ((TextView)v.findViewById(R.id.addressInfo)).setText(marker.getSnippet());
                return v;
            }
        });
        //center the map to include the markers
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 0));
    }

    private void getLocation(){
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
        else
            startUpdatingLocation();


    }

    //make sure user has granted the app permissions to access location
    private void checkPermissions() {
        MultiplePermissionsListener multiplePermissionsListener = new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    getLocation();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        };

        Dexter.checkPermissionsOnSameThread(multiplePermissionsListener, Manifest.permission.ACCESS_FINE_LOCATION);
    }


    private void startUpdatingLocation(){
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        try {
            if(!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                requestLocationDialog();
            }
            else
                LocationServices.FusedLocationApi.requestLocationUpdates(
                        mGoogleApiClient, locationRequest,this);
        }
        //make sure user has turned on location, else request that they turn on location
        catch (SecurityException e){
            checkPermissions();
        }
    }

    private void stopUpdatingLocation(){
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    private void requestLocationDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(MapsActivity.this);
        dialog.setMessage("Please turn on your location.");
        dialog.setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }
        });
        dialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
            }
        });
        dialog.show();
    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            startUpdatingLocation();
        }
        catch(SecurityException e){
            checkPermissions();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            currentLocation = location;

            //if user is in tracking mode, then animate the map as necessary,
            //else do a search for truck stops and then stop updating locations
            if(isTrackingMode) {
                animateMap();
            }
            else{
                searchTruckStops();
                stopUpdatingLocation();
            }
        }
        else{
            if(!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                requestLocationDialog();
            }

        }
    }

    public static void saveToPrefs(Context ctx, String key,String value){
        if(prefManager == null){
            prefManager = ctx.getSharedPreferences("myPrefs",Context.MODE_PRIVATE);
        }
        SharedPreferences.Editor editor = prefManager.edit();
        editor.putString(key,value);
        editor.commit();
    }

    public static String readFromPrefs(Context ctx,String key){
        if(prefManager == null){
            prefManager = ctx.getSharedPreferences("myPrefs",Context.MODE_PRIVATE);
        }
        String g = prefManager.getString(key,"");
        return g;
    }
}
