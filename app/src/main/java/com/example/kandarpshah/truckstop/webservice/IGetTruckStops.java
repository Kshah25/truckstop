package com.example.kandarpshah.truckstop.webservice;

import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Headers;
import retrofit2.http.Path;

/**
 * Created by kandarpshah on 5/22/17.
 */

public interface IGetTruckStops {


    @Headers
            ({RestAdapter.sHeaderAuthToken,RestAdapter.sHeaderContentType})

    @POST("stations/{radius}")
    Call<JsonElement> getTruckStops(@Body TruckStopsRx req, @Path("radius") int radius);
}
