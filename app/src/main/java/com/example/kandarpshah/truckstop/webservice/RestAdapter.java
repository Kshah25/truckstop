package com.example.kandarpshah.truckstop.webservice;

import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kandarpshah on 5/22/17.
 */

public class RestAdapter {

   // public static OkHttpClient client = new OkHttpClient();
    public final static Integer WEBSERVICE_TIMEOUT = 60; // Seconds
    public final static String sHeaderAuthToken = "Authorization: Basic amNhdGFsYW5AdHJhbnNmbG8uY29tOnJMVGR6WmdVTVBYbytNaUp6RlIxTStjNmI1VUI4MnFYcEVKQzlhVnFWOEF5bUhaQzdIcjVZc3lUMitPTS9paU8=";
    public final static String sHeaderContentType = "Content-type: application/json";


  public static Retrofit adapter = null;
    public static Retrofit getRestAdapter()
    {
        if(adapter == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .build();

            adapter = new Retrofit.Builder()
                    .baseUrl(getServerEnvironmentUrl())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                    .build();
        }
        return adapter;
    }

    public static String getServerEnvironmentUrl()
    {
        return "http://webapp.transflodev.com/svc1.transflomobile.com/api/v3/";
    }

    public static String getAuthToken(){
        return "";
    }
}
