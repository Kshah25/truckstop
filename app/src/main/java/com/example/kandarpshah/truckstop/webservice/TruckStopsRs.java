package com.example.kandarpshah.truckstop.webservice;

/**
 * Created by kandarpshah on 5/22/17.
 */

public class TruckStopsRs {

    private String name;
    private String city;
    private String state;
    private String country;
    private String zip;
    private String lat;
    private String lng;
    private String rawLine1;
    private String rawLine2;
    private String rawLine3;

    public Double getLatitude(){
        return Double.valueOf(lat);
    }

    public Double getLongitude(){
        return Double.valueOf(lng);
    }

    public String getName(){
        return name;
    }

    public String getZip(){
        return zip.trim();
    }

    //these functions are used for search matching
    public String getNameLowerCase(){
        return name.toLowerCase().trim();
    }

    public String getCityLowerCase(){
        return city.toLowerCase().trim();
    }

    public String getStateLowerCase(){
        return state.toLowerCase().trim();
    }

    //get truck stop info to display when a marker is clicked on
    public String getSnippet(){
        String snippet = "\n" + rawLine1 + "\n" +
                         city + " " + state + " " + zip + " " + country;

        //only add more info if lines 2 or 3 have content
        if(!rawLine2.isEmpty() || !rawLine3.isEmpty())
            snippet += "\n\nMore Information:" + "\n" + rawLine2 + "\n" + rawLine3;

        return snippet;
    }


}


